# Sollaris

Setting up the project
```
$ git clone https://klynger@bitbucket.org/klynger/solaris.git
$ cd solaris
$ npm install
$ npm run start
```

## Editting pages

To edit pages follow the steps:
1. Go to the project folder
2. Switch to the development branch - ```git checkout dev```
3. Create a new branch - ```git checkout -b <YOUR_BRANCH_NAME>```
4. Make your changes
5. Commit them
6. And then push to the repo with - ```git push origin <YOUR_BRANCH_NAME>```
7. Create a pull request from your branch to the dev branch and wait for the approval
---

### Languange file
To add any changes to texts and images in general of the project you just have to edit the file with all strings in the project that stays at ``/src/lang/pt-BR.json``. The file looks like this

```json
{
  "header_btn_initial_page": "Página Inicial",
  "header_btn_contacts": "Contatos",
  "header_btn_budget": "Orçamento",
  "header_btn_informations": "Informações",
  "header_btn_informations_solar_energy": "O que é energia solar?",
  "header_btn_informations_who_are_we": "Quem somos nós",
  "header_btn_informations_what_we_want": "O que queremos",
  "home_page_slide_data": [
    {
      "img": "/static/images/house1.jpg",
      "alt": "House 1"
    },
    {
      "img": "/static/images/house2.jpg",
      "alt": "House 2"
    },
    {
      "img": "/static/images/house3.jpg",
      "alt": "House 3"
    }
  ]
}
```

If you want to change the text of the button in the header that has route to the home page for example, you just have to change the value of ```"header_btn_initial_page: "<NEW_TEXT_FOR_IT>"```, but if you want to change the slider of the home page just edit ```home_page_slide_data```. If you want to remove an image (the third house for example) you just have to do this:

```json
{
  "home_page_slide_data": [
    {
      "img": "/static/images/house1.jpg",
      "alt": "House 1"
    },
    {
      "img": "/static/images/house2.jpg",
      "alt": "House 2"
    }
  ]
}
```

To add a new image you have to go to the folder ```/public/static/images/<PAGE_NAME>/```, put your images there and make the following editions to the lang file:

```json
{
  "home_page_slide_data": [
    {
      "img": "/static/images/house1.jpg",
      "alt": "House 1"
    },
    {
      "img": "/static/images/house2.jpg",
      "alt": "House 2"
    },
    {
      "img": "/static/images/house3.jpg",
      "alt": "House 3"
    },
    {
      "img": "/static/images/house4.jpg",
      "alt": "This is a house with a garden in from of it"
    }
  ]
}
```

Don't forget to put the commas and always give a nice value to that explains what is in the image.

### Changing Texts

Let's say that you want to change the texts of the page ```/informations/solar-energy```. The text of the page is around this area:

```json
{
  "solar_energy_card_title": "Instalação",
  "solar_energy_card_img_path": "/static/images/solar-energy/solar-energy-card-img.jpg",
  "solar_energy_card_img_alt": "Instalando um painel solar",
  "solar_energy_card_content": "Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica",
  "solar_energy_title": "O que é Energia Solar?",
  "solar_energy_card_aside_content": "0. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  "solar_energy_first_slide_data": [
    {
      "img": "/static/images/solar-energy/house1.jpg",
      "alt": "Casa 1"
    },
    {
      "img": "/static/images/solar-energy/house2.jpg",
      "alt": "Casa 2"
    },
    {
      "img": "/static/images/solar-energy/house3.jpg",
      "alt": "Casa 3"
    }
  ],
  "solar_energy_first_section_title": "Como vemos o futuro",
  "solar_energy_first_section_content": [
    "1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "2. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "3. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
  ],
  "solar_energy_second_section_title": "A energia do futuro",
  "solar_energy_second_section_content": [
    "4. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "5. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "6. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "7. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
  ],
  "solar_energy_second_slide_data": [
    {
      "img": "/static/images/solar-energy/test.jpg",
      "alt": "Imagem teste 1"
    },
    {
      "img": "/static/images/solar-energy/test.jpg",
      "alt": "Imagem teste 2"
    },
    {
      "img": "/static/images/solar-energy/test.jpg",
      "alt": "Imagem teste 3"
    }
  ]
}
```
If you can see there is some arrays with texts and commas seperating them. Every block of text is a paragraph, so you just have to replace this paragraphs with yours to edit the page.

