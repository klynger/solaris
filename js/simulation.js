const $result = document.getElementById('result')

const $inputConsumption = document.getElementById('input-consumption')
const $inputCost = document.getElementById('input-cost')

const $decreaseCost = document.getElementById('descrease-cost')
const $increaseCost = document.getElementById('increase-cost')

const $decreaseCossumption = document.getElementById('descrease-consumption')
const $increaseConsumption = document.getElementById('increase-consumption')

const $constAnual = document.getElementById('cost-anual')
const $costEnergy = document.getElementById('cost-energy')

const $power = document.getElementById('result-power')
const $productionMonth = document.getElementById('result-production-month')
const $qunatityPlacas = document.getElementById('result-qtd-placas')
const $productionYear = document.getElementById('result-production-year')

const $constAnualAtual = document.getElementById('result-anual-cost')
const $economiaAnual = document.getElementById('result-anual-discount')
const $costMonthSolar = document.getElementById('result-cost-month-solar')
const $costTotalSolar = document.getElementById('result-total-cost-solar')

const getKit = power => {
  if (power <= 5) return 6.2 
  else if (power <= 12) return 5.2
  else if (power <= 20) return 4.7
  else if (power <= 30) return 4.4
  else if (power <= 45) return 4.1
  else if (power <= 75) return 3.99
  else return Infinity
}

const toNumber = value => {
  let isFloat = false
  return value.split('').reduce((acc, curr) => {
    if (curr === ',' && !isFloat) {
      isFloat = true
      return acc + ','
    }
    if (!isNaN(curr) && curr !== ' ') return acc + curr
    return acc
  }, '')
}

const beautyNumber = value => {
  let qtyAlgarisms = 0
  let beauty = ''
  for (let i = value.length - 1; i >= 0; i--) {
    if (qtyAlgarisms == 0 && i < value.length - 1) {
      beauty = '.' + beauty
    }
    beauty = value[i] + beauty
    qtyAlgarisms = (qtyAlgarisms + 1) % 3
  }
  return beauty
}

const beautyFloat = (value, addCents) => {
  const number = value.split(',')
  let end = number.length > 1 ? ',' + number[1] : ''
  if (addCents) {
    if (end.length < 1) end = ',00'
    else if (end.length < 3) end += '0'
    else end = ',' + end[1] + end[2]
  }
  return `${beautyNumber(+number[0] + '') || '0'}${end}`
}

const stringFromNumber = value => beautyFloat((value + '').replace('.', ','), true)

const updateResult = data => {
  const { costAnual, costMonthSolar, quantityPlacas, anualEconomy, power, kWh, totalCost } = data
  $constAnual.innerHTML = `R$ ${stringFromNumber(costAnual)}`
  $costEnergy.innerHTML = `R$ ${stringFromNumber(costMonthSolar)}`
  $qunatityPlacas.innerHTML = quantityPlacas
  $constAnualAtual.innerHTML = stringFromNumber(costAnual)
  $costMonthSolar.innerHTML = stringFromNumber(costMonthSolar)
  $economiaAnual.innerHTML = stringFromNumber(anualEconomy)
  $power.innerHTML = stringFromNumber(power)
  $productionMonth.innerHTML = stringFromNumber(kWh)
  $productionYear.innerHTML = stringFromNumber(kWh * 12)
  $costTotalSolar.innerHTML = stringFromNumber(totalCost)
}

const updatePreview = (state => newState => {
  state = { ...state, ...newState }
  const costAnual = state.R$ * 12
  const costMonthSolar = state.R$ * 0.1
  const power = (state.kWh / 29.0) / 5.46
  const quantityPlacas = Math.ceil((power * 1000) / 360.0)
  const anualEconomy = costAnual - (costMonthSolar * 12)
  const totalCost = state.kWh * getKit(power)

  state = {
    ...state,
    costAnual,
    costMonthSolar,
    power,
    quantityPlacas,
    anualEconomy,
    totalCost,
  }

  updateResult(state)
})({ R$: 500, kWh: 500 })

updatePreview({})

const changeCost = (element, prefix, operation) => () => {
  let currentCost = toNumber(element.value || '0')
  currentCost = '0' + currentCost.split(',')[0]
  const newCost = operation(+currentCost)
  element.value = `${prefix} ${beautyNumber(newCost + '')}`
  updatePreview({ [prefix]: newCost })
}

const onChangeCost = (element, prefix) => () => {
  const cost = toNumber(element.value || '0')
  updatePreview({ [prefix]: +cost.replace(',', '.') })
  element.value = `${prefix} ${beautyFloat(cost)}`
}

const redirectToWhatsapp = () => {
  const cost = $inputCost.value
  const consumption = $inputConsumption.value
  const message = `
  Olá!
  Gostaria de fazer o meu orçamento, meu custo médio com energia elétrica de é ${cost} e o meu consumo é de ${consumption}.
  `
  window.open(`https://api.whatsapp.com/send?phone=+5581994690004&text=${message}`, '_blank')
}

$decreaseCost.onclick = changeCost($inputCost, 'R$', a => a > 0 ? a - 10 : a)
$increaseCost.onclick = changeCost($inputCost, 'R$', a => a + 10)
$inputCost.oninput = onChangeCost($inputCost, 'R$')

$decreaseCossumption.onclick = changeCost($inputConsumption, 'kWh', a => a > 0 ? a - 10 : a)
$increaseConsumption.onclick = changeCost($inputConsumption, 'kWh', a => a + 10)
$inputConsumption.oninput = onChangeCost($inputConsumption, 'kWh')
