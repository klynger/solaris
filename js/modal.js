$(document).ready(function() {

  var $videoSrc = "https://www.youtube.com/embed/JTqz_xzozl0";

  // when the modal is opened autoplay it
  $("#myModal").on("shown.bs.modal", function(e) {
    $("#video").attr(
      "src",
      $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0"
    );
  });

  $("#myModal").on("hide.bs.modal", function(e) {
    $("#video").attr("src", $videoSrc);
  });
});
